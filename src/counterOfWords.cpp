#include "counterOfWords.h"
#include <iostream>

int countOfWords(std::string &str){
    int counter = 0;
    std::string::iterator it1 = str.begin();
    std::string::iterator it2 = it1;
    
    while(*it1 == ' '){
        it1++;
    }
    
    str.erase(str.begin(), it1);
    
//    std::cout << "string after erase begin: " << str << std::endl;
    
    it1 = str.end()-1;
    while(*it1 == ' '){
        it1--;
    }
    str.erase(it1+1, str.end());
//    std::cout << "string after erase end: " << str << std::endl;
    
    it1 = str.begin();
    while(*it1 != '\0'){
        it2=it1+1;
        if(*it1 == ' ' && *it2 == ' '){
            str.erase(it1, it2);
            it1--;
        }
        else it1++;
    }
    
//    std::cout << "string after erase between: " << str << std::endl;
    
    it1 = str.begin();
    while(*it1 != '\0'){
        if(*it1 == ' '){
            counter++;
        }
        it1++;
    }
//
//    std::cout << "string-result: " << str << std::endl;
    
    return counter+1;
}
